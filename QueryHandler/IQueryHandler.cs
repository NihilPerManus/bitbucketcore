﻿using BitBucketCore.Query;
using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore.QueryHandler
{
    public interface IQueryHandler<TQuery, TResult> where TQuery : IQuery
    {
        TResult Handle(TQuery query);
    }
}
