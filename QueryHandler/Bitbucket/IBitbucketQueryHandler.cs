﻿using BitBucketCore.Query;
using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore.QueryHandler.Bitbucket
{
    public interface IBitbucketQueryHandler<TQuery, TResult> : IQueryHandler<TQuery,TResult> where TQuery : IQuery 
    {
    }
}
