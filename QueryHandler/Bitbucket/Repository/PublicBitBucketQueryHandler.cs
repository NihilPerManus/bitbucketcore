﻿using BitBucketCore.items;
using BitBucketCore.Query.Bitbucket.Repository;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore.QueryHandler.Bitbucket
{

    

    public class PublicBitBucketQueryHandler : IPublicBitBucketQueryHandler
    {
        RestClient client;

        public PublicBitBucketQueryHandler()
        {
            client = new RestClient(BitBucketCore.Properties.Resources.API_endpoint);
        }


        public RepositoryQueryResults Handle(IBitbucketPublicRepositoryQuery query)
        {
            RestRequest request = new RestRequest($"repositories/{query.Username}", Method.GET);

            return client.Execute<RepositoryQueryResults>(request).Data;
        }
    }

    public interface IPublicBitBucketQueryHandler : IBitbucketQueryHandler<IBitbucketPublicRepositoryQuery, RepositoryQueryResults>
    {
 
    }
}
