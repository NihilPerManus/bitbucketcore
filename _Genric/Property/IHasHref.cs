﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore._Genric.Property
{
    public interface IHasHref
    {
        string Href { get; set; }
    }
}
