﻿using BitBucketCore.items;
using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore._Genric.Property
{
    interface IHasLinks
    {
        Links Links { get; set; }
    }
}
