﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore._Genric.Property
{
    public interface IHasName
    {
        string Name { get; set; }
    }
}
