﻿using BitBucketCore._Genric.Property;
using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore.items
{

    public class RepositoryQueryResults
    {
        public int Pagelen { get; set; }
        public List<RepositoryItem> Values { get; set; }
        public int Page { get; set; }
        public int Size { get; set; }
    }

    public class RepositoryItem : IHasName, IHasLinks
    {
        public string Scm { get; set; }
        public string Website { get; set; }
        public bool Has_wiki { get; set; }
        public string Uuid { get; set; }
        public Links Links { get; set; }
        public string Fork_policy { get; set; }
        public string Name { get; set; }
        public Project Project { get; set; }
        public string Language { get; set; }
        public DateTime Created_on { get; set; }
        public Mainbranch Mainbranch { get; set; }
        public string Full_name { get; set; }
        public bool Has_issues { get; set; }
        public Owner Owner { get; set; }
        public DateTime Updated_on { get; set; }
        public int Size { get; set; }
        public string Type { get; set; }
        public string Slug { get; set; }
        public bool Is_private { get; set; }
        public string Description { get; set; }
    }

    public class Links
    {
        public Watchers Watchers { get; set; }
        public Branches Branches { get; set; }
        public Tags Tags { get; set; }
        public Commits Commits { get; set; }
        public List<Clone> Clone { get; set; }
        public Self Self { get; set; }
        public Source Source { get; set; }
        public Html Html { get; set; }
        public Avatar Avatar { get; set; }
        public Hooks Hooks { get; set; }
        public Forks Forks { get; set; }
        public Downloads Downloads { get; set; }
        public Issues Issues { get; set; }
        public PullRequests PullRequests { get; set; }
    }

    public class Watchers : IHasHref
    {
        public string Href { get; set; }
    }

    public class Branches : IHasHref
    {
        public string Href { get; set; }
    }

    public class Tags : IHasHref
    {
        public string Href { get; set; }
    }

    public class Commits : IHasHref
    {
        public string Href { get; set; }
    }

    public class Self : IHasHref
    {
        public string Href { get; set; }
    }

    public class Source : IHasHref
    {
        public string Href { get; set; }
    }

    public class Html : IHasHref
    {
        public string Href { get; set; }
    }

    public class Avatar : IHasHref
    {
        public string Href { get; set; }
    }

    public class Hooks : IHasHref
    {
        public string Href { get; set; }
    }

    public class Forks : IHasHref
    {
        public string Href { get; set; }
    }

    public class Downloads : IHasHref
    {
        public string Href { get; set; }
    }

    public class Issues : IHasHref
    {
        public string Href { get; set; }
    }

    public class PullRequests : IHasHref
    {
        public string Href { get; set; }
    }

    public class Clone : IHasHref, IHasName
    {
        public string Href { get; set; }
        public string Name { get; set; }
    }

    public class Project : IHasName, IHasLinks
    {
        public string Key { get; set; }
        public string Type { get; set; }
        public string Uuid { get; set; }
        public Links Links { get; set; }
        public string Name { get; set; }
    }


    public class Mainbranch : IHasName
    {
        public string type { get; set; }
        public string Name { get; set; }
    }

    public class Owner : IHasLinks
    {
        public string username { get; set; }
        public string display_name { get; set; }
        public string type { get; set; }
        public string uuid { get; set; }
        public Links Links { get; set; }
    }




}
