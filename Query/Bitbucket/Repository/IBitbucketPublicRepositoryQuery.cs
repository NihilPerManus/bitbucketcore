﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore.Query.Bitbucket.Repository
{

    public class BitbucketPublicRepositoryQuery : IBitbucketPublicRepositoryQuery
    {
        public string Username { get; set; }
    }

    public interface IBitbucketPublicRepositoryQuery : IBitBucketQuery
    {
        string Username { get; set; }
    }
}
