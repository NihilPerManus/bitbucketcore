﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BitBucketCore.Tokens
{
    public class Token
    {
        public string AccessToken { get; set; }
        public string Scopes { get; set; }
        public int ExpiresIn { get; set; }
        public string RefreshToken { get; set; }
        public string TokenType { get; set; }
    }
}
